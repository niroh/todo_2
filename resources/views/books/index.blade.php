<!DOCTYPE html>
<html>

<h1>This is your book list</h1>
<table>
  <tr>
    
    <th>Title</th>
    <th>Author</th>
  </tr>

    @foreach($books as $book)
    <tr>
       
       <td>{{$book->title}}</td>
       <td>{{$book->author}}</td>
    </tr>
    @endforeach

</table>
</html>