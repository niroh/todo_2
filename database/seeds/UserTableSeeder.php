<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                        'name' => 'jack',
                        'email' => 'jack@gmail.com',
                        'password' => '12345678',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                        'name' => 'john',
                        'email' => 'john@gmail.com',
                        'password' => '12345678',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                        'name' => 'ron',
                        'email' => 'ron@gmail.com',
                        'password' => '12345678',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
            
                    ]);
    }
}
