<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
            [
                [
                        'title' => 'Harry Poter',
                        'author' => 'JK Rowlling',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                        'title' => 'A Song Of Ice And Fire',
                        'author' => 'Jeorge RR Martin',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                        'title' => 'The Lord Of The Rings',
                        'author' => 'J.R.R Tolkien',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                        'title' => 'The Hobbit',
                        'author' => 'J.R.R Tolkien',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                       'title' => 'No Friend but the Mountains',
                       'author' => 'Behrouz Boochani',
                       'created_at' => date('Y-m-d G:i:s'),
                ]
                
            
                    ]);
            
    }
}
