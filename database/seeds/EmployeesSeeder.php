<?php

use Illuminate\Database\Seeder;

class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                        'name' => 'Matan Hamburger',
                        'email' => 'matan@gmail.com',
                        'password' => Hash::make('12345678'),
                        'role' => 'employee',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                        'name' => 'Akiva Jacobs',
                        'email' => 'akiva@gmail.com',
                        'password' => Hash::make('12345678'),
                        'role' => 'employee',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                        'name' => 'Yarden Sason Mualem',
                        'email' => 'yarden@gmail.com',
                        'password' => Hash::make('12345678'),
                        'role' => 'employee',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
            
                    ]);
    }
}
