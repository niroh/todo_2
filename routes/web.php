<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/customers/{id?}', function ($id=null) { 
    if ($id!==null)
      return view('customers1',['id'=>$id]);
    else
        return view('customers2');
});

Route::resource('todos', 'TodoController')->middleware('auth');
Route::resource('books', 'BookController');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

